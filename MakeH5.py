import os
import pandas as pd
from root_pandas import read_root
from optparse import OptionParser
from progressbar import ProgressBar

parser = OptionParser()

parser.add_option('-i', '--input-file', type='string', help='Path to input ToyNTuple', dest = "inputFile")
parser.add_option('-o', '--output-dir', type='string', help='Path to place the output', dest = "outputDir")
parser.add_option('-t', '--treeName', type='string', help='Name of the tree', dest = "treeName", default = "ntuple")
parser.add_option('-c', '--columns', type='int', help='Column group', dest = "columns", default = 0)
parser.add_option('--chunk-size', type='int', help='Chunk size', dest = "chunksize", default = -1)

(options, args) = parser.parse_args()

###################################################################

def main(options):
    print("Reading root file: " + options.inputFile)
    channel = os.path.basename(os.path.splitext(options.inputFile)[0])
    columns = None
    chunksize = None
    if options.chunksize > 0: chunksize = options.chunksize

    pbar = ProgressBar()
    os.system("mkdir -p {}".format(options.outputDir))

    n_file = 1
    for df in pbar(read_root(options.inputFile, options.treeName, columns = columns, chunksize=chunksize)):
        df.to_pickle("{}/{}_part{}.h5".format(options.outputDir,channel,n_file))
        print("Output stored in: {}/{}_part{}.h5".format(options.outputDir,channel,n_file))
        n_file += 1


if __name__ == '__main__':
    main(options)
