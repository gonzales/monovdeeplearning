from utils import *
from root_utils import *

# InputFeatures = ['met_tst_et','leadTCC_pt', 'leadTCC_eta', 'leadTCC_phi',
#                 'leadTCC_m', 'leadTCC_tau21', 'leadTCC_D2',
#                 'leadTCC_C2', 'leadTCC_nConstit', 'dPhiTCCJetMet']

# model, scaler, le = loadModel("Models/test/", "_firstTest")

# met_tst_et =          288731.687500
# leadTCC_pt =          229734.796875
# leadTCC_eta =             -0.008477
# leadTCC_phi =              1.215875
# leadTCC_m =            58353.128906
# leadTCC_tau21 =            0.216856
# leadTCC_D2 =               0.899897
# leadTCC_C2 =               0.097835
# leadTCC_nConstit =        16.000000
# dPhiTCCJetMet =            3.124863

# test_row = [met_tst_et,leadTCC_pt,leadTCC_eta,leadTCC_phi,leadTCC_m,leadTCC_tau21,leadTCC_D2,leadTCC_C2,leadTCC_nConstit,dPhiTCCJetMet]
# df2 = pd.DataFrame(np.array([test_row]), columns=InputFeatures)
# print(df2.iloc[0])

# test_x = scaler.fit_transform(df2[InputFeatures].values)
# test_predict = model.predict(test_x, batch_size=256)

# print("prediction:")
# print(test_predict)

####################################################################

# image = np.loadtxt("/nfs/at3-projects/sgonzalez/DL/Images/invH_Preselection_VH/pt/img_n_1_pt_301_met_330.txt")
# image.astype('float32')
# image = image / np.amax(image)
# image = np.expand_dims(image, axis=0)
# image = np.stack(image, axis=2)

# model = loadCNNModel("Models/CNN_GPU_ADAM_bs512_e200_tr70_nds128_dns1_conv1_filt32_kern3_pool2_lr1ppm_b1900ppm_b2999ppm_pat100_cdrop25_ddrop50_aug20/")

# yhatskim_test = model.predict(np.array([image]))
# print("yhatskim_test = {}".format(yhatskim_test))

####################################################################

df = pd.read_pickle("/nfs/at3/projects/sgonzalez/DL/Pickles/invH/Preselection_VH/invH.pkl")

for index, row in df.iterrows():

    hist2d_img = GetTH2DImage(row['leadTCC_eta'], row['leadTCC_phi'], row['TCCJet_lead_clusters_pt'], row['TCCJet_lead_clusters_eta'], row['TCCJet_lead_clusters_phi'])
    array_img = ConvertToArray(hist2d_img)

    array2_img = GetArrayImage(row['leadTCC_eta'], row['leadTCC_phi'], row['TCCJet_lead_clusters_pt'], row['TCCJet_lead_clusters_eta'], row['TCCJet_lead_clusters_phi'])

    if np.array_equal(array_img, array2_img): print("SUCCESS!")


    break

