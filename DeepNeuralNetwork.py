#!/bin/python
from optparse import OptionParser
import os

from utils import *

import numpy as np
import pandas as pd
import sklearn.utils
from bisect import bisect_right
from random import random,choice,seed
from numpy.lib.recfunctions import stack_arrays
import glob

from IPython.display import clear_output, display

from sklearn.preprocessing import StandardScaler,LabelEncoder,MinMaxScaler,OneHotEncoder
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve, auc, roc_auc_score, classification_report
from keras.models import Sequential
from keras.layers import Dense, Dropout, Input
from keras.optimizers import Adam
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.layers.core import Activation
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
    
import math as m    

#****************************************
#Adding Options when runing the code. 
#****************************************
def get_comma_separated_args(option, opt, value, parser):
    setattr(parser.values, option.dest, value.split(','))

def BuildDNN(N_input,width,depth,dropout_rate):
    print("Building model with {} input nodes, {} inner nodes and {} inner layers".format(N_input, width, depth))
    model = Sequential()
    model.add(Dense(units=width, input_dim=N_input))
    model.add(Activation('relu'))
    model.add(Dropout(dropout_rate))

    for i in range(depth):
        # model.add(Dense(int(width/(2*(i+1)))))
        model.add(Dense(width))
        model.add(Activation('relu'))
        # Dropout randomly sets a fraction of input units to 0 at each update during training time
        # which helps prevent overfitting.
        model.add(Dropout(dropout_rate))

    model.add(Dense(1, activation='sigmoid'))

    return model

#######################################
parser = OptionParser()
parser.add_option('-s', '--SigFthFolder', type='string', help='Signal pickle folder path', dest = "sigFth")
parser.add_option('-b', '--BkgFthFolder', type='string', help='Background pickle folder path', dest = "bkgFth")
parser.add_option('--SigName',type='string',help='Label for signal sample',dest='sigName',default="VHinv")
parser.add_option('--BkgName',type='string',help='Label for background sample',dest='bkgName',default='Znunu')
parser.add_option('-n', '--name', type='string', help='Job name') 
parser.add_option('-l', '--label', type='string', help='Label for the name in the plots',default="") 
parser.add_option("-x","--batch-size", dest="batch_size", help="Batch_size", type='int', default=1024)
parser.add_option("-e","--epochs", dest="epochs", help="Epochs", type='int', default=200)
parser.add_option("--patience", dest="patience", help="Patience", type='int', default=5)
parser.add_option("--train-size", dest="train_size", help="train_size", type='float', default=0.7)
parser.add_option("--nodes", dest="nodes", help="Nodes per layer", type='int', default=80)
parser.add_option("--depth", dest="depth", help="Number of inner layers", type='int', default=1)
parser.add_option("--dropout", dest="dropout_rate", help="Dropout rate", type='float', default=0.2)
parser.add_option('-f',"--features", dest="features", help="Feature index", type='int', default=0)

parser.add_option("--do-plots", action="store_true", dest="do_plots", help="do variable plots" , default=False)
parser.add_option("--lr", dest="LR", help="learning rate", type='float', default=0.001)
parser.add_option("--load-model", dest="load_model", help="Path to folder with the model to load", type="string", default="")

parser.add_option('-w', '--use-weights', dest = "use_weights", action="store_true", help='Use weights when training', default=False)
parser.add_option('-r',"--reweight-amount", dest="reweight_amount", help="Amount to reweight sig and bkg", type='float', default=1)

(options, args) = parser.parse_args()

def DeepNeuralNetwork(options):

    sig_df_paths = glob.glob("{}/*.feather".format(options.sigFth))
    bkg_df_paths = glob.glob("{}/*.feather".format(options.bkgFth))

    sig_sample = getNNSample(sig_df_paths, options.sigName)
    if options.use_weights:
        bkg_sample = getNNSample(bkg_df_paths, options.bkgName) # Load all bkg if use_weights since it will be rescaled
    else :
        bkg_sample = getNNSample(bkg_df_paths, options.bkgName, sig_sample['df'].shape[0]) # Load same amount of bkg as sig if weights are not used

    sig_sample['df'].insert(0,'isSignal',1)
    bkg_sample['df'].insert(0,'isSignal',0)

    InputFeatures = GetFeatures(options.features)

    # if 'leadTCC_eta' in InputFeatures:
    #     sig_sample['df']['leadTCC_eta'] = np.fabs(sig_sample['df']['leadTCC_eta'])
    #     bkg_sample['df']['leadTCC_eta'] = np.fabs(bkg_sample['df']['leadTCC_eta'])

    if options.use_weights:
        print("======================================")
        reweight_amount = options.reweight_amount
        print("Rescaling training weights to {}".format(reweight_amount))
        sumW_sig = sum(sig_sample['df']['weight'])
        sumW_bkg = sum(bkg_sample['df']['weight'])
        print("Sum of signal before = {}".format(sumW_sig))
        print("Sum of background before = {}".format(sumW_bkg))
        sig_factor = reweight_amount / sumW_sig
        bkg_factor = reweight_amount / sumW_bkg
        sig_sample['df']['weight'] *= sig_factor
        bkg_sample['df']['weight'] *= bkg_factor
        print("Sum of signal = {}".format(sum(sig_sample['df']['weight'])))
        print("Sum of background = {}".format(sum(bkg_sample['df']['weight'])))
        
    signal_and_background = [sig_sample['df'],bkg_sample['df']]
    signal_and_background = pd.concat(signal_and_background,ignore_index=True, sort=False)   
    signal_and_background = signal_and_background.dropna(axis=0, subset = InputFeatures)
    signal_and_background = Randomizing(signal_and_background) 

    print("======================================")
    print("Signal shape = {}".format(sig_sample['df'].shape))
    print("Bkg shape = {}".format(bkg_sample['df'].shape))
    print("Total events = {}".format(signal_and_background.shape))
    print("Events removed from NaN = {}".format(sig_sample['df'].shape[0] + bkg_sample['df'].shape[0] - signal_and_background.shape[0]))
    print("======================================")

    if options.do_plots:
        os.system("mkdir -p Plots/TrainingFeatures")
        os.system("mkdir -p Plots/TrainingFeatures/{}".format(options.name))
        PlotTrainingFeatures(signal_and_background, InputFeatures, "Plots/TrainingFeatures/{}".format(options.name), options.label, options.sigName, options.bkgName)

    # make the X array
    Xskim=signal_and_background[InputFeatures].values
    #make y vector
    y_tmpskim=signal_and_background['isSignal']
    # make the event weights vector
    wskim=signal_and_background['weight']


    if (options.load_model == ""):

        scaler = MinMaxScaler(feature_range = (0, 1))
        le = LabelEncoder()
        Xskim = scaler.fit_transform(Xskim)
        yskim = le.fit_transform(y_tmpskim)


        Xskim_train, Xskim_test, yskim_train, yskim_test, wskim_train, wskim_test = train_test_split(Xskim, yskim, wskim, train_size=options.train_size,random_state=123)


        print("======================================")
        print("Train set shape: {}".format(Xskim_train.shape))
        print("Test set shape: {}".format(Xskim_test.shape))
        print("Total data shape: {}".format(Xskim.shape))
        print("======================================")

        n_dim=Xskim_train.shape[1]
        n_nodes = options.nodes
        n_depth = options.depth
        dropout_rate = options.dropout_rate

        # os.system('mkdir -p Models/{}{}'.format(options.name,options.label))
        model=BuildDNN(n_dim,n_nodes,n_depth,dropout_rate)

        adam = Adam(lr=options.LR, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
        model.compile(loss='mean_squared_error',optimizer=adam,metrics=['accuracy'])

        # callbacks = [EarlyStopping(verbose=True, patience=options.patience, monitor='loss'), ModelCheckpoint('Models/{}{}/model.h5'.format(options.name,options.label), monitor='loss', verbose=True, save_best_only=True, mode='max')]
        callbacks = [EarlyStopping(monitor='val_loss', patience=options.patience, restore_best_weights=True), ModelCheckpoint('Models/{}{}/model.h5'.format(options.name,options.label), monitor='loss', verbose=True, save_best_only=True, mode='max')]
    
        #We train the model
        print("Start training with a BATCH SIZE of {}".format(options.batch_size))
        if options.use_weights:
            modelMetricsHistoryskim = model.fit(Xskim_train, yskim_train, sample_weight = wskim_train.values, batch_size=options.batch_size, epochs=options.epochs, verbose=2, validation_data=(Xskim_test,yskim_test,wskim_test), callbacks=callbacks)
        else:
            modelMetricsHistoryskim = model.fit(Xskim_train, yskim_train, batch_size=options.batch_size, epochs=options.epochs, verbose=2, validation_data=(Xskim_test,yskim_test), callbacks=callbacks)

        saveModel(model,scaler,le,options.name,options.label,modelMetricsHistoryskim)

        perf = model.evaluate(Xskim_test, yskim_test)
        print('Test loss: {}'.format(perf[0]))
        print('Test accuracy: {}'.format(perf[1]))
        plotTrainingValidation(modelMetricsHistoryskim, "Plots/{}/{}{}/TraningLoss".format(options.name, options.name, options.label), 'loss', 'b')
        plotTrainingValidation(modelMetricsHistoryskim, "Plots/{}/{}{}/TraningAccuracy".format(options.name, options.name, options.label), 'accuracy', 'r')

    else:
        model, scaler, le = loadModel(options.load_model)

        Xskim = scaler.fit_transform(Xskim)
        yskim = le.fit_transform(y_tmpskim)

        Xskim_train, Xskim_test, yskim_train, yskim_test, wskim_train, wskim_test = train_test_split(Xskim, yskim, wskim, train_size=options.train_size,random_state=123)
    

    #==================================================================================

    print("=================================================")
    AUC = doROCCurve(model, Xskim_test, yskim_test, options.name, options.label)
    T_cut = doDiscriminant(model, Xskim_test, yskim_test, Xskim_train, yskim_train, options.sigName, options.bkgName, options.name, options.label)
    plotConfusionMatrix(model, Xskim_test, yskim_test,"Plots/{}/{}{}/ConfusionMatrix".format(options.name, options.name, options.label))

    return {
            'AUC':                              AUC,
            'T_cut':                            T_cut,
            }
    
def main(options):

    os.system('mkdir -p Plots')
    os.system('mkdir -p Models')

    options.label += "_bs{}_e{}_tr{}_nds{}_dpt{}_lr{}ppm_pat{}_drop{}_f{}".format(options.batch_size,options.epochs,int(100*options.train_size),options.nodes,options.depth,int(1000*options.LR),options.patience,int(100*options.dropout_rate),options.features)
    if options.use_weights: options.label += "_useW_r{}pc".format(int(100*options.reweight_amount))

    os.system('mkdir -p Plots/{}'.format(options.name))
    os.system('mkdir -p Plots/{}/{}{}'.format(options.name, options.name, options.label))

    out = DeepNeuralNetwork(options)
    
    print ("DONE!")

if __name__ == '__main__':
    main(options)
