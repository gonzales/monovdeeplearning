import os
import numpy as np
import pandas as pd
import math as m    
import sklearn.utils
import matplotlib.pyplot as plt
import glob

import tensorflow as tf
import keras

from keras.models import load_model
from keras.utils.vis_utils import plot_model
from sklearn.metrics import confusion_matrix
import joblib
import random
from sklearn.metrics import roc_curve, auc, roc_auc_score, classification_report
import seaborn as sn


# -- unit conversion units, and image parameters
GeV = 1000
DegToRad = np.pi / 180.
step = 0.05
N = int(1/step)

#########################################################

def ApplyPreselection(df):
    df_preselection = df.loc[(df['met_fire'] == 1) & (df['n_el_baseline'] == 0) & (df['n_mu_baseline'] == 0) & (df['n_TCCJet'] >= 1) & (df['dPhiTCCJetMet'] > 120*DegToRad) & (df['n_trackTCCSeparatedBjet'] == 0) & (df['met_tst_et'] > 250*GeV)]
    return df_preselection

def ApplyPreselectionMergedFile(df):
    df_preselection = df.loc[(((df['year'] == 2015) & (df['trigger_HLT_xe70_mht'] == 1)) | ((df['year'] == 2016) & ((df['trigger_HLT_xe90_mht_L1XE50'] == 1) | (df['trigger_HLT_xe100_mht_L1XE50'] == 1) | (df['trigger_HLT_xe110_mht_L1XE50'] == 1) | (df['trigger_HLT_xe110_mht_L1XE50'] == 1)))) & (df['n_el_baseline'] == 0) & (df['n_mu_baseline'] == 0) & (df['tau_loose_multiplicity'] == 0) & (df['n_LCTopoJet'] >= 1) & (df['dPhiLCTopoJetMet'] > 120*DegToRad) & (df['n_trackLCTopoSeparatedBjet'] == 0) & (df['met_tst_et'] > 250*GeV)]
    return df_preselection

def Randomizing(df):
    df = sklearn.utils.shuffle(df,random_state=123) #'123' is the random seed
    df = df.reset_index(drop=True)# drop=True does not allow the reset_index to insert a new column with the suffled index
    return df

def getSample(df_files, name):
    return {'df_files' : df_files, 'name' : name}

def getNNSample(df_files, name, n_rows = -1):
    rows = []
    columns = []
    n_rows_perFile = m.floor(n_rows/len(df_files)) if n_rows > 0 else -1

    dfs = []
    
    for i_file,df_path in enumerate(df_files):
        print("Opening {}  (File {}/{})".format(df_path,i_file+1,len(df_files)))

        # df = ApplyPreselection(Randomizing(pd.read_feather(df_path)))
        df = pd.read_feather(df_path)
        columns = df.columns
        rows_added = 0
        for index, row in df.iterrows():
            rows.append(row)
            rows_added += 1
            if n_rows_perFile > 0 and rows_added >= n_rows_perFile: break

    df_NN = Randomizing(pd.DataFrame(rows, columns=columns))
    return {'df' : df_NN, 'name' : name}

def GetFeatures(feature_index):
    InputFeatures = []
    if feature_index == 0 : InputFeatures = ['met_tst_et','leadTCC_pt', 'leadTCC_eta', 'leadTCC_phi', 'leadTCC_m', 'leadTCC_tau21', 'leadTCC_D2', 'leadTCC_C2', 'leadTCC_nConstit', 'dPhiTCCJetMet']
    if feature_index == 1 : InputFeatures = ['leadTCC_D2','leadTCC_m','leadTCC_nConstit','leadTCC_tau21']
    if feature_index == 2 : InputFeatures = ['met_tst_et','leadTCC_pt', 'leadTCC_m', 'leadTCC_tau21', 'leadTCC_D2', 'leadTCC_C2', 'leadTCC_nConstit', 'dPhiTCCJetMet']
    if feature_index == 3 : InputFeatures = ['met_tst_et','leadLCTopoJet_pt', 'leadLCTopoJet_eta', 'leadLCTopoJet_phi', 'leadLCTopoJet_m', 'leadLCTopoJet_tau21', 'leadLCTopoJet_D2', 'leadLCTopoJet_C2', 'leadLCTopoJet_nConstit', 'dPhiLCTopoJetMet']
    return InputFeatures

def PlotTrainingFeatures(signal_and_background, InputFeatures, outDir, label, sigName, bkgName):

    signal_df = signal_and_background.loc[signal_and_background["isSignal"] == 1]
    bkg_df = signal_and_background.loc[signal_and_background["isSignal"] == 0]

    # n_bins, xlow, xhigh, factor, xLabel
    hist_info = {
                'met_tst_et' :          [120,0,1200,1./GeV,"MET [GeV]"],
                'leadTCC_pt' :          [120,0,1200,1./GeV,"TCC Jet pT [GeV]"],
                'leadTCC_eta' :         [40,-3.5,3.5,1,"TCC Jet eta"],
                'leadTCC_phi' :         [40,-3.5,3.5,1,"TCC Jet phi"],
                'leadTCC_m' :           [20,0,200,1./GeV,"TCC Jet m [GeV]"],
                'leadTCC_tau21' :       [50,0,1,1,"TCC Jet tau21"],
                'leadTCC_D2' :          [50,0,10,1,"TCC Jet D2"],
                'leadTCC_C2' :          [50,0,1,1,"TCC Jet C2"],
                'leadTCC_nConstit' :    [50,0,50,1,"TCC Jet nConstit"],
                'dPhiTCCJetMet' :       [50,0,3.5,1,"DeltaPhi(TCC,Met)"],
            }

    for var in InputFeatures:
        bins = np.linspace(hist_info[var][1], hist_info[var][2] , hist_info[var][0])
        plt.hist(signal_df[var] * hist_info[var][3], weights=signal_df['weight'], histtype='step', density=True, bins=bins, label=sigName, linewidth=2)
        plt.hist(bkg_df[var] * hist_info[var][3], weights=bkg_df['weight'], histtype='step', density=True, bins=bins, label=bkgName, linewidth=2)

        plt.xlabel(hist_info[var][4])
        plt.yscale('log')
        plt.legend(loc='best')
        plt.savefig("{}/{}{}.png".format(outDir, var, label))
        plt.clf()
        print("Plot stored: {}/{}{}.png".format(outDir, var, label))


def SaveArray(array, fileName):
    np.savetxt(fileName, array)

def saveModel(model,scaler,le,name,label,modelMetricsHistoryskim):
    os.system("mkdir -p Models/{}".format(name))
    os.system("mkdir -p Models/{}/{}{}".format(name,name,label))
    model.save('Models/{}/{}{}/model.h5'.format(name,name,label))
    joblib.dump(scaler, "Models/{}/{}{}/scaler.save".format(name,name,label))
    joblib.dump(le, "Models/{}/{}{}/le.save".format(name,name,label))
    joblib.dump(modelMetricsHistoryskim.history, "Models/{}/{}{}/history.save".format(name,name,label))

def loadModel(path_to_model):
    if path_to_model == "": return None, None, None
    print("Loading model from " + path_to_model)
    model = load_model('{}/model.h5'.format(path_to_model))
    scaler = joblib.load('{}/scaler.save'.format(path_to_model))
    le = joblib.load('{}/le.save'.format(path_to_model))
    return model, scaler, le

def plotModel(model, fileName):
    plot_model(model, to_file='{}.png'.format(fileName), show_shapes=True, show_layer_names=True)
    print("Model plot stored in {}.png".format(fileName))

def saveCNNModel(model,name,label,modelMetricsHistoryskim):
    os.system("mkdir -p Models/{}".format(name))
    os.system("mkdir -p Models/{}/{}{}".format(name,name,label))
    model.save('Models/{}/{}{}/model.h5'.format(name,name,label))
    joblib.dump(modelMetricsHistoryskim.history, "Models/{}/{}{}/history.save".format(name,name,label))

def loadCNNModel(path_to_model):
    print("Loading model from " + path_to_model)
    # model = load_model('{}/model.h5'.format(path_to_model))
    model = tf.keras.models.load_model('{}/model.h5'.format(path_to_model))
    return model

def preprocessImage(image):
    image.astype('float32')
    image = image / np.amax(image)
    image = np.expand_dims(image, axis=0) # Some redimensionalitation required for CNN training
    image = np.stack(image, axis=2) # Some redimensionalitation required for CNN training
    return image

def getCNNImages(path_to_images, channel, train_size, predict_label, max_images = -1, dataAugFract = 0):
    random.seed(123)
    files = glob.glob("{}/{}/*".format(path_to_images, channel))
    if max_images == -1: max_images = len(files)
    images = []
    train = []
    test = []
    n_images = 0
    print("Loading images from {}/{}".format(path_to_images, channel))
    for i_file,file in enumerate(files):
        if i_file % 1000 == 0 or i_file == len(files) - 1: print("Loading image {}/{}".format(i_file+1,len(files)))
        image = np.loadtxt(file)
        image = preprocessImage(image)
        # image.astype('float32')
        # image = image / np.amax(image)
        # image = np.expand_dims(image, axis=0) # Some redimensionalitation required for CNN training
        # image = np.stack(image, axis=2) # Some redimensionalitation required for CNN training
        if n_images < int(max_images*train_size):
            train.append(image)
            if dataAugFract > 0 and random.random() < dataAugFract:
                if random.random() < 0.5: train.append(flipImageVertically(image))
                else: train.append(flipImageHorizontally(image))
        else:
            test.append(image)
        n_images += 1
        # images.append(image)
        if n_images >= max_images: break
    train = np.array(train)
    test = np.array(test)

    # print("SWITCHING TRAIN AND TEST!!")
    # bkp_train = train
    # train = np.array(test)
    # test = np.array(bkp_train)

    y_train = predict_label * np.ones(len(train))
    y_test = predict_label * np.ones(len(test))

    return {'train':train, 'test':test, 'y_train':y_train, 'y_test':y_test}

def flipImageVertically(image): # image.shape (40,40,1)
    n_row = image.shape[0]
    n_col = image.shape[1]
    new_image = np.zeros(image.shape)
    for row in range(n_row):
        for col in range(n_col): 
            new_image[row][col] = image[n_row - row - 1][col]
    return new_image

def flipImageHorizontally(image): # image.shape (40,40,1)
    n_row = image.shape[0]
    n_col = image.shape[1]
    new_image = np.zeros(image.shape)
    for row in range(n_row):
        for col in range(n_col): 
            new_image[row][col] = image[row][n_col - col - 1]
    return new_image

def shuffle_images(sig, bkg, sig_y, bkg_y):
    # random.seed(555)
    x = np.concatenate((sig, bkg))
    y = np.concatenate((sig_y, bkg_y))
    z = list(zip(x,y))
    random.shuffle(z)
    x, y = zip(*z)
    return (np.array(x), np.array(y))

def doROCCurve(model, Xskim_test, yskim_test, name, label):
    # Generates output predictions for the input samples.
    yhatskim_test = model.predict(Xskim_test)
    # Get 'Receiver operating characteristic' (ROC)
    FPR, TPR, thresholds = roc_curve(yskim_test, yhatskim_test)

    # Compute Area Under the Curve (AUC) from prediction scores
    AUC  = auc(FPR, TPR)
    print("ROC AUC: %0.5f" % AUC)

    plt.plot(FPR, TPR, color='darkorange',  lw=2, label='Full curve (area = %0.4f)' % AUC)
    plt.plot([0, 0], [1, 1], color='navy', lw=2, linestyle='--')
    plt.xlim([-0.05, 1.0])
    plt.ylim([0.0, 1.05])
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
    plt.title('ROC curves for Signal vs Background')
    plt.legend(loc="lower right")

    plt.savefig("Plots/{}/{}{}/ROC_Curve.png".format(name,name,label))
    plt.clf()

    print("ROC Curve plot stored in Plots/{}/ROC_Curve{}.png".format(name,label))

    return AUC

def getTcut(n_sig, n_bkg, bins): #important
    alpha = np.cumsum(n_sig/len(bins))
    beta = 1 - np.cumsum(n_bkg/len(bins))
    signal_to_noise_ratio = []
    for i in range(len(alpha)):
        if beta[i] > 0:
            signal_to_noise_ratio.append((1. - alpha[i])/m.sqrt(beta[i]))
        else:
            signal_to_noise_ratio.append(0)
    T_cut = bins[np.argmax(signal_to_noise_ratio)]
    return {'T_cut':T_cut,'alpha':alpha,'beta':beta,'signal_to_noise_ratio':signal_to_noise_ratio}

def doDiscriminant(model, Xskim_test, yskim_test, Xskim_train, yskim_train, sigName, bkgName, name, label, useTCut = False):
    n_bins = 100 # Granularity to compute the T_cut
    n_bins_plt = 100 # Binning to use in the plot

    signal_test = Xskim_test[yskim_test == 1]
    bkg_test = Xskim_test[yskim_test == 0]
    signal_train = Xskim_train[yskim_train == 1]
    bkg_train = Xskim_train[yskim_train == 0]

    yhat_test_sig = model.predict(signal_test)
    yhat_test_bkg = model.predict(bkg_test)
    yhat_train_sig = model.predict(signal_train)
    yhat_train_bkg = model.predict(bkg_train)

    n_sig, bins = np.histogram(yhat_test_sig, bins = n_bins, density = True, range=(0,1))
    n_bkg, bins = np.histogram(yhat_test_bkg, bins = n_bins, density = True, range=(0,1))

    cut_info = getTcut(n_sig, n_bkg, bins)
    T_cut = cut_info['T_cut']
    if not useTCut: T_cut = 0.5

    plt.hist(yhat_train_bkg, histtype='step', density=True, label=bkgName + " (train)", linestyle="--", linewidth=0, range=(0,1),bins=n_bins_plt,fill=True, edgecolor=None, facecolor='r', alpha=0.25)
    plt.hist(yhat_train_sig, histtype='step', density=True, label=sigName + " (train)", linestyle="--", linewidth=0, range=(0,1),bins=n_bins_plt,fill=True, edgecolor=None, facecolor='g', alpha=0.25)
    plt.hist(yhat_test_bkg, histtype='step', density=True, label=bkgName + " (test)", edgecolor='darkred', linewidth=2, range=(0,1),bins=n_bins_plt, facecolor=None)
    plt.hist(yhat_test_sig, histtype='step', density=True, label=sigName + " (test)", edgecolor='darkgreen', linewidth=2, range=(0,1),bins=n_bins_plt, facecolor=None)
    plt.axvline(x=T_cut,color='black',label=r'$T_{cut}$' + ' = {:.2f}'.format(T_cut), linestyle='-.')
    if useTCut:
        plt.plot(bins[:-1], 1 - cut_info['alpha'], label=r"$1 - \alpha$")
        plt.plot(bins[:-1], cut_info['beta'], label=r"$\beta$")
        plt.plot(bins[:-1], cut_info['signal_to_noise_ratio'], label=r"$(1 - \alpha)/\sqrt{\beta}$")
    plt.xlim(0.,1)
    plt.yscale('log')
    plt.legend(loc='upper center')
    plt.xlabel('NN score')

    os.system('mkdir -p Discriminant/{}'.format(name))
    os.system('mkdir -p Discriminant/{}/{}{}'.format(name, name, label))
    plt.savefig("Discriminant/{}/{}{}/Discriminant.png".format(name,name,label))
    # plt.yscale('linear')
    # plt.savefig("Discriminant/{}/Discriminant{}_linearScale.png".format(name, label))
    plt.clf()

    print("Discriminant plot stored in Discriminant/{}/Discriminant{}.png".format(name,label))

    return T_cut

def plotTrainingValidation(modelMetricsHistoryskim, fileName, key, color):
    values=modelMetricsHistoryskim.history[key]
    val_values=modelMetricsHistoryskim.history['val_'+key]

    epochs = range(1,len(values)+1)
    plt.plot(epochs, values, color + "o", label="Training " + key)
    plt.plot(epochs, val_values, color, label="Validation " + key)
    plt.legend(loc=0)
    plt.xlabel("Epochs")
    plt.ylabel(key.capitalize())
    plt.savefig("{}.png".format(fileName))
    plt.clf()
    print("{} plot stored in {}.png".format(key.capitalize(),fileName))

def plotConfusionMatrix(model, x_test, y_test, fileName, cut = 0.5):
    y_predict = model.predict(x_test)
    map_func = np.vectorize(lambda y: 1 if y > cut else 0)
    y_test = map_func(y_test)
    y_predict = map_func(y_predict)

    cm = confusion_matrix(y_test, y_predict)

    df_cm = pd.DataFrame(cm, index = ['Actual 0','Actual 1'],
                  columns = ['Predicted 0', 'Predicted 1'])
    plt.figure(figsize = (10,7))
    ax = sn.heatmap(df_cm, annot=True, fmt='d')

    for t in ax.texts:
        t.set_text(t.get_text() + "\n({0:.2f} %)".format(100 * int(t.get_text())/len(y_test)))

    plt.title("Accuracy = {0:.2f} %".format( 100 * (cm[0][0] + cm[1][1]) / len(y_test) ))

    plt.savefig("{}.png".format(fileName))
    plt.clf()
    print("Confusion matrix plot stored in {}.png".format(fileName))
