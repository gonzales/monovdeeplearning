#!/bin/python

import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D, ZeroPadding2D
from keras import backend as K


from optparse import OptionParser
import numpy as np
import os
import random
import math as m
from sklearn.metrics import roc_curve, auc, roc_auc_score, classification_report
from keras.callbacks import EarlyStopping, ModelCheckpoint

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from utils import *

def get_comma_separated_args(option, opt, value, parser):
    setattr(parser.values, option.dest, value.split(','))

#######################################
parser = OptionParser()

# parser.add_option('-p', '--path',type='string',help='Path to folder where the folder images are', dest = "parentFolder", default="/nfs/pic.es/user/s/sgonzalez/scratch2/DL/BernatJetImages_perChannel/")
parser.add_option('-s', '--signalImages',type='string',help='Signal path to images folder', dest = "sigFolder", default="invH") #actually you should put either invH for sig and Znunu for bkg
parser.add_option('-b', '--bkgImages',type='string',help='Bkg path to images folder', dest = "bkgFolder", default="Znunu")
parser.add_option('--SigName',type='string',help='Label for signal sample',dest='sigName',default="VHinv")
parser.add_option('--BkgName',type='string',help='Label for background sample',dest='bkgName',default='Znunu')
parser.add_option('-n', '--name', type='string', help='Job name') 
parser.add_option('-l', '--label', type='string', help='Label for the name in the plots',default="") 
parser.add_option("-x","--batch-size", dest="batch_size", help="Batch_size", type='int', default=128)
parser.add_option("-e","--epochs", dest="epochs", help="Epochs", type='int', default=200)
parser.add_option("--train-size", dest="train_size", help="train_size", type='float', default=0.7)
# parser.add_option("-m","--maxFiles", dest="max_files", help="Maximum number of files you want to use", type='int', default=100000)
# parser.add_option("-w","--weights", dest="weights", help="Use weights (1) or not (0)", type='int', default=0)
# parser.add_option('--param-file', dest = "param_file", type='string', help='File to write all the parameters', default="NoFile")
# parser.add_option('-c', '--channels', type='string', help='Comma separated channel names', action='callback', callback=get_comma_separated_args)
# parser.add_option("--do-normalise-per-image", action="store_true", dest="norm_per_image", help="Normalise each image instad of all images at the same time" , default=False)
# parser.add_option("--disableWeights", action="store_true", dest="disableWeights", help="Disable weights" , default=False)
parser.add_option("--nodes",dest="nodes",type='int',help="Number of nodes in the dense inner layers",default=128)
parser.add_option("--dense_layers",dest="dense_layers",type='int',help="Number of 1D Layers",default=1)
parser.add_option("--conv_layers",dest="conv_layers",type='int',help="Number of Conv2D, MaxPooling layers",default=1)
parser.add_option("--filters",dest="filters",type='int',help="Number of filters",default=32)
parser.add_option("--kernel_size",dest="kernel",type='int',help="Kernel size",default=3)
parser.add_option("--pool_size",dest="pool_size",type='int',help="Pooling size",default=2)
parser.add_option("--lr", dest="LR", help="learning rate", type='float', default=0.01)
parser.add_option("--rho", dest="rho", help="rho (optimizer parameter)", type='float', default=0.95)
parser.add_option("--beta_1", dest="beta_1", help="beta_1 (optimizer parameter)", type='float', default=0.9)
parser.add_option("--beta_2", dest="beta_2", help="beta_2 (optimizer parameter)", type='float', default=0.999)
parser.add_option("--patience", dest="patience", help="Patience", type='int', default=5)
parser.add_option("--conv_dropout_rate", dest="conv_dropout_rate", help="Dropout rate in CNN", type='float', default=0.25)
parser.add_option("--dense_dropout_rate", dest="dense_dropout_rate", help="Dropout rate in dense layers", type='float', default=0.5)
parser.add_option("--max-images", dest="max_images", help="Max number of images to use per sample", type='int', default=-1)
parser.add_option('--dataAugmentation', dest = "dataAugmentation", type='float', help='Augment a fraction of train data', default=0)
parser.add_option("--doParamOptimization", action="store_true", dest="doParamOptimization", help="Do the combinations for the parameter optimization study" , default=False)


(options, args) = parser.parse_args()

def buildCNN(input_shape, filters, kernel, conv_layers, pool_size, dense_layers, nodes, conv_dropout_rate, dense_dropout_rate):
    model = Sequential()
    print("input shape is {}".format(input_shape))
    model.add(Conv2D(filters, kernel_size=(kernel,kernel),activation='relu',input_shape=input_shape))
    # model.add(MaxPooling2D(pool_size=(pool_size,pool_size)))
    for n in range(conv_layers):
        model.add(Conv2D(filters, (kernel,kernel), activation='relu'))
        model.add(MaxPooling2D(pool_size=(pool_size,pool_size)))
        model.add(Dropout(conv_dropout_rate))
    model.add(Flatten())
    for n in range(dense_layers):
        model.add(Dense(nodes,activation='relu'))
        model.add(Dropout(dense_dropout_rate))
    model.add(Dense(1, activation='sigmoid'))
    return model


def CNN(options, signal, background, channels):

    ## BUILD LABEL
    options.label += "_bs{}_e{}_tr{}_nds{}_dns{}_conv{}_filt{}_kern{}_pool{}_lr{}ppm_b1{}ppm_b2{}ppm_pat{}_cdrop{}_ddrop{}".format(options.batch_size,options.epochs,int(100*options.train_size),options.nodes,options.dense_layers,options.conv_layers,options.filters,options.kernel,options.pool_size,int(1000*options.LR),int(1000*options.beta_1),int(1000*options.beta_2),options.patience,int(100*options.conv_dropout_rate),int(100*options.dense_dropout_rate))
    if options.max_images > 0:
        options.label += "_m{}".format(options.max_images)
    if options.dataAugmentation > 0:
        options.label += "_aug{}".format(int(100*options.dataAugmentation))
    print("LABEL = {}".format(options.label))
    
    os.system('mkdir -p Models/{}{}'.format(options.name,options.label))
    os.system('mkdir -p Plots/{}'.format(options.name))
    os.system('mkdir -p Plots/{}/{}{}'.format(options.name,options.name, options.label))

    ## SPLIT DATASET
    (x_train, y_train) = shuffle_images(signal['train'], background['train'], signal['y_train'], background['y_train'])
    (x_test, y_test) = shuffle_images(signal['test'], background['test'], signal['y_test'], background['y_test'])

    print("sig_train shape {}".format(signal['train'].shape))
    print("sig_test shape {}".format(signal['test'].shape))
    print("bkg_train shape {}".format(background['train'].shape))
    print("bkg_test shape {}".format(background['test'].shape))
    print("x_train shape {}".format(x_train.shape))
    print("x_test shape {}".format(x_test.shape))
    print("=============================================================================================================================")
    img_rows, img_cols = x_train.shape[1], x_train.shape[2]
    
    if K.image_data_format() == 'channels_first':
        input_shape = (len(channels), img_rows, img_cols)
    else:
        input_shape = (img_rows, img_cols, len(channels))

    model = buildCNN(input_shape, options.filters, options.kernel, options.conv_layers, options.pool_size, options.dense_layers, options.nodes, options.conv_dropout_rate, options.dense_dropout_rate)
    # adadelta = keras.optimizers.Adadelta(lr=options.LR, rho=options.rho)
    # model.compile(loss="binary_crossentropy",optimizer=adadelta,metrics=['accuracy'])
    adam = keras.optimizers.Adam(lr=options.LR, beta_1=options.beta_1, beta_2=options.beta_2, epsilon=None, decay=0.0, amsgrad=False)
    model.compile(loss='binary_crossentropy',optimizer=adam,metrics=['accuracy'])
    print("Start model fit")
    # callbacks = [EarlyStopping(verbose=True, patience=options.patience, monitor='loss'), ModelCheckpoint('Models/{}{}/model.h5'.format(options.name,options.label), monitor='loss', verbose=True, save_best_only=True, mode='max')]
    callbacks = [EarlyStopping(monitor='val_loss', patience=options.patience, restore_best_weights=True), ModelCheckpoint('Models/{}/{}{}/model.h5'.format(options.name,options.name,options.label), monitor='loss', verbose=True, save_best_only=True, mode='max')]
    
    modelMetricsHistoryskim = model.fit(x_train, y_train, batch_size=options.batch_size, epochs=options.epochs, verbose=2, validation_data=(x_test,y_test), callbacks=callbacks)
    # modelMetricsHistoryskim = model.fit(x_train, y_train, epochs=options.epochs,batch_size=options.batch_size,validation_split=0.0,callbacks=callbacks, verbose=2)
    saveCNNModel(model,options.name,options.label, modelMetricsHistoryskim)

    print("========================================================================")
    print("Start model evaluation")
    perf = model.evaluate(x_test, y_test)
    print('Test loss: {}'.format(perf[0]))
    print('Test accuracy: {}'.format(perf[1]))

    AUC = doROCCurve(model, x_test, y_test, options.name, options.label)
    T_cut = doDiscriminant(model, x_test, y_test, x_train, y_train, options.sigName, options.bkgName, options.name, options.label)
    plotTrainingValidation(modelMetricsHistoryskim, "Plots/{}/{}{}/TraningLoss".format(options.name, options.name, options.label), 'loss', 'b')
    plotTrainingValidation(modelMetricsHistoryskim, "Plots/{}/{}{}/TraningAccuracy".format(options.name, options.name, options.label), 'accuracy', 'r')
    plotConfusionMatrix(model, x_test, y_test, "Plots/{}/{}{}/ConfusionMatrix".format(options.name, options.name, options.label))

def main(options):

    os.system('mkdir -p Plots')
    os.system('mkdir -p Models')

    ############
    channels = ['pt']
    print("=============================================================================================================================")
    print("Reading signal data from: {}".format(options.sigFolder))
    print("Reading bkg data from: {}".format(options.bkgFolder))
    print("Channels used are {}".format(channels))
    print("=============================================================================================================================")

    signal = getCNNImages(options.sigFolder, channels[0], options.train_size, 1, options.max_images, options.dataAugmentation)
    background = getCNNImages(options.bkgFolder, channels[0], options.train_size, 0, options.max_images, options.dataAugmentation)
    ############

    if options.doParamOptimization:
        v_batchSize = [128,512]
        v_epochs = [200]
        v_trainsize = [0.7]
        v_nodes=[128]
        v_dense_layers=[1]
        v_conv_layers=[1]
        v_filters=[32]
        v_kernel_size=[3]
        v_pool_size=[2]
        v_learningRate=[0.01]
        v_rho=[0.95]
        v_patience=[5]
        v_conv_dropout_rate=[0.25]
        v_dense_dropout_rate=[0.5]

        orig_label = options.label

        combinations = np.array(np.meshgrid(v_batchSize,v_epochs,v_trainsize,v_nodes,v_dense_layers,v_conv_layers,v_filters,v_kernel_size,v_pool_size,v_learningRate,v_rho,v_patience,v_conv_dropout_rate,v_dense_dropout_rate)).T.reshape(-1,14)

        for combination in combinations:
            options.batch_size = int(combination[0])
            options.epochs = int(combination[1])
            options.train_size = combination[2]
            options.nodes = int(combination[3])
            options.dense_layers = int(combination[4])
            options.conv_layers = int(combination[5])
            options.filters = int(combination[6])
            options.kernel = int(combination[7])
            options.pool_size = int(combination[8])
            options.LR = combination[9]
            options.rho = combination[10]
            options.patience = int(combination[11])
            options.conv_dropout_rate = combination[12]
            options.dense_dropout_rate = combination[13]
            options.label = orig_label

            out = CNN(options, signal, background, channels)

    else:
        out = CNN(options, signal, background, channels)

    
    print ("DONE!")

if __name__ == '__main__':
    main(options)

