import os
import glob
from ROOT import gROOT, TFile, TH1D, TH2D, TCanvas
import pandas as pd
from root_pandas import read_root
from optparse import OptionParser
from HistTools import *

parser = OptionParser()

parser.add_option('-i', '--inputPklFolder', type='string', help='Path to folder with pickles', dest = "inputPklFolder")
parser.add_option('-n', '--name', type='string', help='Name', dest = "name")
parser.add_option('-o', '--outDir', type='string', help='Path to place output', dest = "outDir")
parser.add_option('-m', '--max', type='int', help='Max number of images', dest = "max_images", default = -1)
parser.add_option("--do-plots", action="store_true", dest="do_plots", help="do plot some images" , default=False)


(options, args) = parser.parse_args()

###################################################################

def main(options):
    print("Input folder: {}".format(options.inputPklFolder))
    df_files = glob.glob("{}/*.pkl".format(options.inputPklFolder))

    os.system("mkdir -p {}".format(options.outDir))
    os.system("mkdir -p {}/{}".format(options.outDir, options.name))
    os.system("mkdir -p {}/{}/pt".format(options.outDir, options.name))
    os.system("mkdir -p {}/{}/weight".format(options.outDir, options.name))
    os.system("mkdir -p {}/{}/img".format(options.outDir, options.name))

    i_image = 0

    max_files_per_file = -1 if options.max_images < 0 else options.max_images / len(df_files)

    for i_file,df_path in enumerate(df_files):
        print("Opening {}  (File {}/{})".format(df_path,i_file+1,len(df_files)))

        df = Randomizing(pd.read_pickle(df_path))
        i_event = 0
        n_events_preselection = df.shape[0]
        n_image_per_file = 0
        for index, row in df.iterrows():
            i_event += 1
            if i_event % 10000 == 0 or i_event == n_events_preselection - 1: print("Processing event {} ({:.2f} %)".format(i_event, 100*(i_event+1)/n_events_preselection))
            if row['n_TCCJet'] == 0: continue
            i_image += 1
            n_image_per_file += 1

            # if i_image % 50001 == 1:
            #     folder = "from{}_to{}".format(i_image, i_image + 49999)
            #     os.system("mkdir -p {}/{}/pt/{}".format(options.outDir, options.name, folder))
            #     os.system("mkdir -p {}/{}/weight/{}".format(options.outDir, options.name, folder))

            name_img = "img_n_{}_pt_{}_met_{}".format(i_image, int(row['leadTCC_pt']/GeV), int(row['met_tst_et']/GeV))
            file_img_pt = "{}/{}/pt/{}.txt".format(options.outDir,options.name, name_img)
            file_img_w = "{}/{}/weight/{}.txt".format(options.outDir,options.name, name_img)

            if os.path.exists(file_img_pt): continue

            hist2d_img = GetTH2DImage(row['leadTCC_eta'], row['leadTCC_phi'], row['TCCJet_lead_clusters_pt'], row['TCCJet_lead_clusters_eta'], row['TCCJet_lead_clusters_phi'])
            array_img = ConvertToArray(hist2d_img)

            if options.do_plots and i_image <= 100: plot2DHistogram(hist2d_img, "eta", "phi", "{}/{}/img/{}".format(options.outDir,options.name,name_img))
            SaveArray(array_img, file_img_pt)
            os.system("echo \"{}\" > {}".format(row['weight'], file_img_w))

            if max_files_per_file > 0 and n_image_per_file >= max_files_per_file: break

    print("DONE!")

if __name__ == '__main__':
    main(options)
