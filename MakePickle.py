import os
import pandas as pd
from root_pandas import read_root
from optparse import OptionParser
from progressbar import ProgressBar
from utils import *

parser = OptionParser()

parser.add_option('-i', '--input-file', type='string', help='Path to input ToyNTuple', dest = "inputFile")
parser.add_option('-o', '--output-dir', type='string', help='Path to place the output', dest = "outputDir")
parser.add_option('-t', '--treeName', type='string', help='Name of the tree', dest = "treeName", default = "ntuple")
parser.add_option('-c', '--columns', type='int', help='Column group', dest = "columns", default = 0)
parser.add_option('-s', '--selection', type='string', help='Selection to apply', dest = "selection", default = "")
parser.add_option('--chunk-size', type='int', help='Chunk size', dest = "chunksize", default = -1)

(options, args) = parser.parse_args()

###################################################################

def main(options):
    print("Reading root file: " + options.inputFile)
    channel = os.path.basename(os.path.splitext(options.inputFile)[0])
    columns = None
    chunksize = None if options.chunksize == -1 else options.chunksize
    if options.chunksize > 0: chunksize = options.chunksize

    pbar = ProgressBar()
    os.system("mkdir -p {}".format(options.outputDir))

    if options.selection != "":
        options.outputDir += "/{}".format(options.selection) 
        os.system("mkdir -p {}".format(options.outputDir))

    if chunksize:
        n_file = 1
        for df in pbar(read_root(options.inputFile, options.treeName, columns = columns, chunksize=chunksize)):
            
            processAndSave(df, options.selection, "{}/{}_part{}.pkl".format(options.outputDir,channel,n_file))
            n_file += 1
    else:
        df = read_root(options.inputFile, options.treeName, columns = columns)
        processAndSave(df, options.selection, "{}/{}.pkl".format(options.outputDir,channel))

    print("------------------------------")
    print("DONE!")

def processAndSave(df, selection, fileName):

    leadTCC_pt = []
    leadTCC_eta = []
    leadTCC_phi = []
    leadTCC_m = []
    leadTCC_D2 = []
    leadTCC_C2 = []
    leadTCC_tau21 = []
    leadTCC_nConstit = []

    if "Preselection" in selection:
        df = ApplyPreselection(df)
    if "VH" in selection:
        df = df.loc[(df['run'] == 346605) | (df['run'] == 346606) | (df['run'] == 346607)]

    for index, row in df.iterrows():
        leadTCC_pt.append(getLeadingTCC(row,"pt"))
        leadTCC_eta.append(getLeadingTCC(row,"eta"))
        leadTCC_phi.append(getLeadingTCC(row,"phi"))
        leadTCC_m.append(getLeadingTCC(row,"m"))
        leadTCC_D2.append(getLeadingTCC(row,"D2"))
        leadTCC_C2.append(getLeadingTCC(row,"C2"))
        leadTCC_tau21.append(getLeadingTCC(row,"tau21"))
        leadTCC_nConstit.append(getLeadingTCC(row,"nConstit"))

    df.insert(0,'leadTCC_pt',leadTCC_pt)
    df.insert(0,'leadTCC_eta',leadTCC_eta)
    df.insert(0,'leadTCC_phi',leadTCC_phi)
    df.insert(0,'leadTCC_m',leadTCC_m)
    df.insert(0,'leadTCC_D2',leadTCC_D2)
    df.insert(0,'leadTCC_C2',leadTCC_C2)
    df.insert(0,'leadTCC_tau21',leadTCC_tau21)
    df.insert(0,'leadTCC_nConstit',leadTCC_nConstit)

    df.to_pickle(fileName)
    print("Output stored in: {}".format(fileName))

def getLeadingTCC(row,attr):
    if (row['n_TCCJet'] == 0):
        return -999
    else:
        return row['TCCJet_' + attr][0]

if __name__ == '__main__':
    main(options)
