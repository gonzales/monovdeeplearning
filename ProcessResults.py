import os
import glob
from ROOT import gROOT, TFile, TH1D, TH2D, TCanvas
import pandas as pd
from root_pandas import read_root
from optparse import OptionParser
from HistTools import *
from math import sqrt

from tabulate import tabulate

parser = OptionParser()

# parser.add_option('--VH', type='string', help='Path to Plots folder for VH', dest = "VH_plots")
parser.add_option('--invH', type='string', help='Path to Plots folder for invH', dest = "invH_plots")
parser.add_option('--Znunu  ', type='string', help='Path to Plots folder for Znunu', dest = "Znunu_plots")
parser.add_option('-l',"--lumi", dest="lumi", help="Luminosity pb-1", type='float', default=1)
parser.add_option('-p', '--plotDir', type='string', help='Path to plots', dest = "plotDir")

# parser.add_option('-n', '--name', type='string', help='Name', dest = "name")
# parser.add_option('-d',"--load-DNNmodel", dest="load_DNNmodel", help="Path to folder with the DNN model to load", type="string", default="")
# parser.add_option('-f',"--features", dest="features", help="Feature index", type='int', default=0)

(options, args) = parser.parse_args()

###################################################################

def main(options):
    # print("VH folder: {}".format(options.VH_plots))
    print("invH folder: {}".format(options.invH_plots))
    print("Znunu folder: {}".format(options.Znunu_plots))

    histTools_VH = getHistTools("VH",getHistToolsChannelPath(options.invH_plots,'VH'))
    histTools_ggF = getHistTools("ggF",getHistToolsChannelPath(options.invH_plots,'ggF'))
    histTools_VBF = getHistTools("VBF",getHistToolsChannelPath(options.invH_plots,'VBF'))
    histTools_ggZH = getHistTools("ggZH",getHistToolsChannelPath(options.invH_plots,'ggZH'))
    histTools_ttH = getHistTools("ttH",getHistToolsChannelPath(options.invH_plots,'ttH'))
    histTools_invH = getHistTools("invH",options.invH_plots)
    histTools_Znunu = getHistTools("Znunu",options.Znunu_plots)

    print("------------- Plotting ---------------")
    os.system("mkdir -p {}".format(options.plotDir))

    allPlots(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu, options.plotDir, options.lumi)

    print("------------- Results ---------------")
    # results_VH = getAnalysisResults(histTools_VH, options.lumi)
    results_invH = getAnalysisResults(histTools_invH, options.lumi)
    results_ggF = getAnalysisResults(histTools_ggF, options.lumi)
    results_VBF = getAnalysisResults(histTools_VBF, options.lumi)
    results_VH = getAnalysisResults(histTools_VH, options.lumi)
    results_ggZH = getAnalysisResults(histTools_ggZH, options.lumi)
    results_ttH = getAnalysisResults(histTools_ttH, options.lumi)
    results_Znunu = getAnalysisResults(histTools_Znunu, options.lumi)

    print(tabulate([
                        ['VTagger', results_ttH['VTagger'][2], results_ggZH['VTagger'][2], results_VH['VTagger'][2], results_VBF['VTagger'][2], results_ggF['VTagger'][2], results_invH['VTagger'][2], results_Znunu['VTagger'][2],'', results_ttH['VTagger'][2]/sqrt(results_Znunu['VTagger'][2]), results_ggZH['VTagger'][2]/sqrt(results_Znunu['VTagger'][2]), results_VH['VTagger'][2]/sqrt(results_Znunu['VTagger'][2]), results_VBF['VTagger'][2]/sqrt(results_Znunu['VTagger'][2]), results_ggF['VTagger'][2]/sqrt(results_Znunu['VTagger'][2]), results_invH['VTagger'][2]/sqrt(results_Znunu['VTagger'][2])], 
                        ['DNN', results_ttH['DNN'][2], results_ggZH['DNN'][2], results_VH['DNN'][2], results_VBF['DNN'][2], results_ggF['DNN'][2], results_invH['DNN'][2], results_Znunu['DNN'][2],'',results_ttH['DNN'][2]/sqrt(results_Znunu['DNN'][2]), results_ggZH['DNN'][2]/sqrt(results_Znunu['DNN'][2]), results_VH['DNN'][2]/sqrt(results_Znunu['DNN'][2]), results_VBF['DNN'][2]/sqrt(results_Znunu['DNN'][2]), results_ggF['DNN'][2]/sqrt(results_Znunu['DNN'][2]), results_invH['DNN'][2]/sqrt(results_Znunu['DNN'][2])],
                        ['CNN', results_ttH['CNN'][2], results_ggZH['CNN'][2], results_VH['CNN'][2], results_VBF['CNN'][2], results_ggF['CNN'][2], results_invH['CNN'][2], results_Znunu['CNN'][2],'',results_ttH['CNN'][2]/sqrt(results_Znunu['CNN'][2]), results_ggZH['CNN'][2]/sqrt(results_Znunu['CNN'][2]), results_VH['CNN'][2]/sqrt(results_Znunu['CNN'][2]), results_VBF['CNN'][2]/sqrt(results_Znunu['CNN'][2]), results_ggF['CNN'][2]/sqrt(results_Znunu['CNN'][2]), results_invH['CNN'][2]/sqrt(results_Znunu['CNN'][2])]
                    ], 
                    headers=['', 'ttH', 'ggZH', 'VH', 'VBF', 'ggF', 'invH','Znunu','','S(ttH)/sqrt(Znunu)','S(ggZH)/sqrt(Znunu)','S(VH)/sqrt(Znunu)','S(VBF)/sqrt(Znunu)','S(ggF)/sqrt(Znunu)','S(invH)/sqrt(Znunu)'], 
                    tablefmt='orgtbl'))

    print("-----------------------------------------")
    print(tabulate([
                        ['VTagger', results_ttH['VTagger'][0], results_ggZH['VTagger'][0], results_VH['VTagger'][0], results_VBF['VTagger'][0], results_ggF['VTagger'][0], results_invH['VTagger'][0], results_Znunu['VTagger'][0],'', results_ttH['VTagger'][0]/results_Znunu['VTagger'][1], results_ggZH['VTagger'][0]/results_Znunu['VTagger'][1], results_VH['VTagger'][0]/results_Znunu['VTagger'][1], results_VBF['VTagger'][0]/results_Znunu['VTagger'][1], results_ggF['VTagger'][0]/results_Znunu['VTagger'][1], results_invH['VTagger'][0]/results_Znunu['VTagger'][1]], 
                        ['DNN', results_ttH['DNN'][0], results_ggZH['DNN'][0], results_VH['DNN'][0], results_VBF['DNN'][0], results_ggF['DNN'][0], results_invH['DNN'][0], results_Znunu['DNN'][0],'',results_ttH['DNN'][0]/results_Znunu['DNN'][1], results_ggZH['DNN'][0]/results_Znunu['DNN'][1], results_VH['DNN'][0]/results_Znunu['DNN'][1], results_VBF['DNN'][0]/results_Znunu['DNN'][1], results_ggF['DNN'][0]/results_Znunu['DNN'][1], results_invH['DNN'][0]/results_Znunu['DNN'][1]],
                        ['CNN', results_ttH['CNN'][0], results_ggZH['CNN'][0], results_VH['CNN'][0], results_VBF['CNN'][0], results_ggF['CNN'][0], results_invH['CNN'][0], results_Znunu['CNN'][0],'',results_ttH['CNN'][0]/results_Znunu['CNN'][1], results_ggZH['CNN'][0]/results_Znunu['CNN'][1], results_VH['CNN'][0]/results_Znunu['CNN'][1], results_VBF['CNN'][0]/results_Znunu['CNN'][1], results_ggF['CNN'][0]/results_Znunu['CNN'][1], results_invH['CNN'][0]/results_Znunu['CNN'][1]]
                    ], 
                    headers=['', 'ttH', 'ggZH', 'VH', 'VBF', 'ggF', 'invH','Znunu','','S(ttH)/Znunu_err','S(ggZH)/Znunu_err','S(VH)/Znunu_err','S(VBF)/Znunu_err','S(ggF)/Znunu_err','S(invH)/Znunu_err'], 
                    tablefmt='orgtbl'))

    print("-----------------------------------------")
    print("DONE!")

def getHistToolsChannelPath(invHPath, channel):
    return invHPath.replace(os.path.basename(invHPath),os.path.basename(invHPath)+"_{}".format(channel))

def getHistTools(name, loadDir):
    histTools_skim = HistTools(name + "_skim", "")
    histTools_skim.loadHistograms(loadDir + "_skim/histograms.root")
    histTools_VTagger = HistTools(name + "_VTagger", "")
    histTools_VTagger.loadHistograms(loadDir + "_VTagger/histograms.root")
    histTools_DNN = HistTools(name + "_DNN", "")
    histTools_DNN.loadHistograms(loadDir + "_DNN/histograms.root")
    histTools_CNN = HistTools(name + "_CNN", "")
    histTools_CNN.loadHistograms(loadDir + "_CNN/histograms.root")
    return {'skim':histTools_skim, 'VTagger':histTools_VTagger, 'DNN':histTools_DNN, 'CNN':histTools_CNN}

def getAnalysisResults(channel_histTools, lumi):
    yield_skim = channel_histTools['skim'].hists["Yield"].hist.GetBinContent(1) * lumi
    yield_VTagger = channel_histTools['VTagger'].hists["Yield"].hist.GetBinContent(1) * lumi
    yield_DNN = channel_histTools['DNN'].hists["Yield"].hist.GetBinContent(1) * lumi
    yield_CNN = channel_histTools['CNN'].hists["Yield"].hist.GetBinContent(1) * lumi
    err_skim = channel_histTools['skim'].hists["Yield"].hist.GetBinError(1) * lumi
    err_VTagger = channel_histTools['VTagger'].hists["Yield"].hist.GetBinError(1) * lumi
    err_DNN = channel_histTools['DNN'].hists["Yield"].hist.GetBinError(1) * lumi
    err_CNN = channel_histTools['CNN'].hists["Yield"].hist.GetBinError(1) * lumi
    raw_skim = channel_histTools['skim'].hists["Yield"].hist.GetEntries()
    raw_VTagger = channel_histTools['VTagger'].hists["Yield"].hist.GetEntries()
    raw_DNN = channel_histTools['DNN'].hists["Yield"].hist.GetEntries()
    raw_CNN = channel_histTools['CNN'].hists["Yield"].hist.GetEntries()
    return {'skim':[yield_skim, err_skim, raw_skim], 'VTagger':[yield_VTagger, err_VTagger, raw_VTagger], 'DNN':[yield_DNN, err_DNN, raw_DNN], 'CNN':[yield_CNN, err_CNN, raw_CNN]}

def plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu, strategy, plotKey, xLabel, fileName, lumi):
    hists = []
    hists.append(histTools_Znunu[strategy].getHistogram(plotKey))
    hists.append(histTools_VH[strategy].getHistogram(plotKey))
    hists.append(histTools_ggF[strategy].getHistogram(plotKey))
    hists.append(histTools_invH[strategy].getHistogram(plotKey))
    lumi_xstr = 0.13
    lumi_ystr = 0.85
    if strategy == 'skim':
        lumi_xstr = 0.65
        lumi_ystr = 0.85
    stackHistograms(hists, xLabel, ['Znunu','VH','ggF','invH'], [EColor.kRed, EColor.kGreen, EColor.kCyan, EColor.kBlue], False, "{}".format(fileName), lumi, ['hist','p','p','p'], lumi_xstr, lumi_ystr)
    stackHistograms(hists, xLabel, ['Znunu','VH','ggF','invH'], [EColor.kRed, EColor.kGreen, EColor.kCyan, EColor.kBlue], True, "{}_norm".format(fileName), lumi, ['hist','p','p','p'], lumi_xstr, lumi_ystr)

def allPlots(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu, plotDir, lumi):
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'skim', "DNN Discriminant", "DNN score", "{}/DNN_Discriminant_skim".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'skim', "CNN Discriminant", "CNN score", "{}/CNN_Discriminant_skim".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'skim','met_tst_et',"MET [GeV]", "{}/met_tst_et_skim".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'skim','leadTCC_pt', "TCC Jet pT [GeV]", "{}/leadTCC_pt_skim".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'skim','leadTCC_eta',"TCC Jet eta", "{}/leadTCC_eta_skim".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'skim','leadTCC_phi',"TCC Jet phi", "{}/leadTCC_phi_skim".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'skim','leadTCC_m',"TCC Jet m [GeV]", "{}/leadTCC_m_skim".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'skim','leadTCC_tau21',"TCC Jet tau21", "{}/leadTCC_tau21_skim".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'skim','leadTCC_D2',"TCC Jet D2", "{}/leadTCC_D2_skim".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'skim','leadTCC_C2',"TCC Jet C2", "{}/leadTCC_C2_skim".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'skim','leadTCC_nConstit',"TCC Jet nConstit", "{}/leadTCC_nConstit_skim".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'skim','dPhiTCCJetMet',"DeltaPhi(TCC,Met)", "{}/dPhiTCCJetMet_skim".format(plotDir), lumi)

    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'VTagger', "DNN Discriminant", "DNN score", "{}/DNN_Discriminant_VTagger".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'VTagger', "CNN Discriminant", "CNN score", "{}/CNN_Discriminant_VTagger".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'VTagger','met_tst_et',"MET [GeV]", "{}/met_tst_et_VTagger".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'VTagger','leadTCC_pt', "TCC Jet pT [GeV]", "{}/leadTCC_pt_VTagger".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'VTagger','leadTCC_eta',"TCC Jet eta", "{}/leadTCC_eta_VTagger".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'VTagger','leadTCC_phi',"TCC Jet phi", "{}/leadTCC_phi_VTagger".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'VTagger','leadTCC_m',"TCC Jet m [GeV]", "{}/leadTCC_m_VTagger".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'VTagger','leadTCC_tau21',"TCC Jet tau21", "{}/leadTCC_tau21_VTagger".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'VTagger','leadTCC_D2',"TCC Jet D2", "{}/leadTCC_D2_VTagger".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'VTagger','leadTCC_C2',"TCC Jet C2", "{}/leadTCC_C2_VTagger".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'VTagger','leadTCC_nConstit',"TCC Jet nConstit", "{}/leadTCC_nConstit_VTagger".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'VTagger','dPhiTCCJetMet',"DeltaPhi(TCC,Met)", "{}/dPhiTCCJetMet_VTagger".format(plotDir), lumi)

    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'DNN', "DNN Discriminant", "DNN score", "{}/DNN_Discriminant_DNN".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'DNN', "CNN Discriminant", "CNN score", "{}/CNN_Discriminant_DNN".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'DNN','met_tst_et',"MET [GeV]", "{}/met_tst_et_DNN".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'DNN','leadTCC_pt', "TCC Jet pT [GeV]", "{}/leadTCC_pt_DNN".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'DNN','leadTCC_eta',"TCC Jet eta", "{}/leadTCC_eta_DNN".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'DNN','leadTCC_phi',"TCC Jet phi", "{}/leadTCC_phi_DNN".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'DNN','leadTCC_m',"TCC Jet m [GeV]", "{}/leadTCC_m_DNN".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'DNN','leadTCC_tau21',"TCC Jet tau21", "{}/leadTCC_tau21_DNN".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'DNN','leadTCC_D2',"TCC Jet D2", "{}/leadTCC_D2_DNN".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'DNN','leadTCC_C2',"TCC Jet C2", "{}/leadTCC_C2_DNN".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'DNN','leadTCC_nConstit',"TCC Jet nConstit", "{}/leadTCC_nConstit_DNN".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'DNN','dPhiTCCJetMet',"DeltaPhi(TCC,Met)", "{}/dPhiTCCJetMet_DNN".format(plotDir), lumi)

    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'CNN', "DNN Discriminant", "DNN score", "{}/DNN_Discriminant_CNN".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'CNN', "CNN Discriminant", "CNN score", "{}/CNN_Discriminant_CNN".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'CNN','met_tst_et',"MET [GeV]", "{}/met_tst_et_CNN".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'CNN','leadTCC_pt', "TCC Jet pT [GeV]", "{}/leadTCC_pt_CNN".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'CNN','leadTCC_eta',"TCC Jet eta", "{}/leadTCC_eta_CNN".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'CNN','leadTCC_phi',"TCC Jet phi", "{}/leadTCC_phi_CNN".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'CNN','leadTCC_m',"TCC Jet m [GeV]", "{}/leadTCC_m_CNN".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'CNN','leadTCC_tau21',"TCC Jet tau21", "{}/leadTCC_tau21_CNN".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'CNN','leadTCC_D2',"TCC Jet D2", "{}/leadTCC_D2_CNN".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'CNN','leadTCC_C2',"TCC Jet C2", "{}/leadTCC_C2_CNN".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'CNN','leadTCC_nConstit',"TCC Jet nConstit", "{}/leadTCC_nConstit_CNN".format(plotDir), lumi)
    plotResults(histTools_VH, histTools_ggF, histTools_VBF, histTools_invH, histTools_Znunu,'CNN','dPhiTCCJetMet',"DeltaPhi(TCC,Met)", "{}/dPhiTCCJetMet_CNN".format(plotDir), lumi)

if __name__ == '__main__':
    main(options)
