from optparse import OptionParser

parser = OptionParser()

parser.add_option('-i', '--input-folder', type='string', help='Path to input folder', dest = "inputFolder")
parser.add_option('-o', '--output-dir', type='string', help='Path to place the output', dest = "outputDir")

(options, args) = parser.parse_args()

import os
import glob
import pandas as pd

def main(options):
    print("Reading pickle files from: " + options.inputFolder)
    channel = os.path.basename(os.path.normpath(options.inputFolder))
    files = glob.glob("{}/*pkl".format(options.inputFolder))
    os.system("mkdir -p " + options.outputDir)

    for i_file,file in enumerate(files):
        print("----------------------------------")
        print("Processing file {} ({}/{})".format(file, i_file + 1, len(files)))
        df = pd.read_pickle(file)
        df.reset_index(inplace=True)
        df.to_feather(os.path.splitext(file)[0] + ".feather")
        print("File stored in " + os.path.splitext(file)[0] + ".feather")

if __name__ == '__main__':
    main(options)
