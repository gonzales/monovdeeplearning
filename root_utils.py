import os
import numpy as np
from ROOT import gROOT, gStyle, TFile, TH1D, TH2D, TCanvas, TLegend, EColor, THStack, TLatex
import pandas as pd
import math as m    
import sklearn.utils
import matplotlib.pyplot as plt
import glob
from keras.models import load_model
from sklearn.externals import joblib
import random
from sklearn.metrics import roc_curve, auc, roc_auc_score, classification_report


# -- unit conversion units, and image parameters
GeV = 1000
DegToRad = np.pi / 180.
step = 0.05
N = int(1/step)

gROOT.SetBatch(True)
gStyle.SetOptStat(0)

#########################################################

def getCanvas():
    c = TCanvas("","",int(700*1.618), 700)
    c.SetLogy()
    c.SetLeftMargin(0.1)
    c.SetRightMargin(0.15)
    c.SetBottomMargin(0.11)
    return c

def getDefaultLegend():
    legend = TLegend(0.86,0.4,1.1,0.6)
    legend.SetMargin(0.05)
    legend.SetBorderSize(0)
    legend.SetFillColor(10)
    legend.SetTextSize(0.045)
    return legend

def drawLumiLabel(lumi, xstr = 0.13, ystr = 0.85) :
    latex = TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.040)
    latex.SetTextAlign(13)
    latex.SetTextColor(EColor.kBlack)
    lumiText = "#int L dt = {:.2f}".format(lumi) + "fb^{-1}"
    latex.DrawLatex(xstr, ystr, lumiText)

def plotHistogram(hist, xLabel, fileName):
    c = getCanvas()
    c.cd()
    hist.GetXaxis().SetTitle(xLabel)
    hist.Draw("hist")
    c.Print("{}.png".format(fileName))

def plot2DHistogram(hist, xLabel, yLabel, fileName):
    c = TCanvas("","",700,700)
    c.cd()
    c.SetLeftMargin(0.15)
    c.SetRightMargin(0.15)
    hist.GetXaxis().SetTitle(xLabel)
    hist.GetYaxis().SetTitle(yLabel)
    hist.Draw("colz")
    c.Print("{}.png".format(fileName))

def stackHistograms(hists, xLabel, legends, colors, normalise, fileName, scaleFactor = 1, drawStyles = [], lumi_xstr = 0.13, lumi_ystr = 0.85):
    c = getCanvas()
    c.cd()
    legend = getDefaultLegend()
    hs = THStack("","")
    hs_fill = THStack("","")
    for index,hist in enumerate(hists):
        try:
            clone = hist.Clone(hist.GetName())
        except:
            print("Error when plotting in stackHistograms for {}".format(fileName))
            return
        if normalise: clone.Scale(1./clone.Integral("width"))
        clone.Scale(scaleFactor)
        clone.SetLineWidth(3)
        clone.SetLineColor(colors[index])
        clone.SetFillColorAlpha(colors[index],0.7)
        if len(drawStyles) == 0:
            hs.Add(clone)
        else:
            hs.Add(clone, drawStyles[index])
        legend.AddEntry(clone, legends[index],"le")
    hs.Draw("nostack")
    hs.GetXaxis().SetTitle(xLabel)
    hs.GetXaxis().SetTitleSize(0.04)
    legend.Draw()
    if scaleFactor != 1:
        drawLumiLabel(scaleFactor / 1000, lumi_xstr, lumi_ystr) 
    c.Print("{}.png".format(fileName))


def Get2DBin(hist, x, y):
    xaxis = hist.GetXaxis()
    yaxis = hist.GetYaxis()
    binx = xaxis.FindBin(x)
    biny = yaxis.FindBin(y)
    return hist.GetBin(binx,biny)

def GetTH2DImage(TCCJet_eta, TCCJet_phi, clusters_pt, clusters_eta, clusters_phi):
    hist2D = TH2D("", "", 2*N, -1, 1, 2*N, -1, 1)
    for i_cluster in range(0,len(clusters_pt)):
        d_eta = clusters_eta[i_cluster] - TCCJet_eta
        d_phi = clusters_phi[i_cluster] - TCCJet_phi
        if d_phi > np.pi: d_phi = -2*np.pi + d_phi
        if d_phi < -1*np.pi: d_phi = 2*np.pi - d_phi

        bin = Get2DBin(hist2D, d_eta, d_phi)
        hist2D.SetBinContent(bin, hist2D.GetBinContent(bin) + clusters_pt[i_cluster]/GeV)
    
    return hist2D

def GetArrayImage(TCCJet_eta, TCCJet_phi, clusters_pt, clusters_eta, clusters_phi):
    array = np.zeros((2*N,2*N))
    for i_cluster in range(0,len(clusters_pt)):
        d_eta = clusters_eta[i_cluster] - TCCJet_eta
        d_phi = clusters_phi[i_cluster] - TCCJet_phi
        if d_phi > np.pi: d_phi = -2*np.pi + d_phi
        if d_phi < -1*np.pi: d_phi = 2*np.pi - d_phi

        i_eta = m.floor((d_eta + 1.) / step)
        i_phi = m.floor((d_phi + 1.) / step)

        if i_phi >= 2*N or i_phi < 0: continue
        if i_eta >= 2*N or i_phi < 0: continue

        array[2*N - i_phi - 1][i_eta] += clusters_pt[i_cluster]/GeV

    return array

def ConvertToArray(hist):
    array = []
    for c_phi in np.arange(1-step/2., -1, -1*step):
        row = []
        for c_eta in np.arange(-1 + step/2., 1, step):
            bin = Get2DBin(hist, c_eta, c_phi)
            row.append(hist.GetBinContent(bin))
        array.append(row)
    return np.array(array)

