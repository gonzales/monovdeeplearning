import os
import glob
from ROOT import gROOT, TFile, TH1D, TH2D, TCanvas
import pandas as pd
from root_pandas import read_root
from optparse import OptionParser
from HistTools import *

parser = OptionParser()

parser.add_option('-i', '--inputPklFolder', type='string', help='Path to folder with pickles', dest = "inputPklFolder")
parser.add_option('-n', '--name', type='string', help='Name', dest = "name")
parser.add_option('-p', '--plotDir', type='string', help='Path to plots', dest = "plotDir")
parser.add_option('-d',"--load-DNNmodel", dest="load_DNNmodel", help="Path to folder with the DNN model to load", type="string", default="")
parser.add_option('-c',"--load-CNNmodel", dest="load_CNNmodel", help="Path to folder with the CNN model to load", type="string", default="")
parser.add_option('-f',"--features", dest="features", help="Feature index", type='int', default=0)
parser.add_option('--split-channels', dest = "split_channels", action="store_true", help='Split to invH channels', default=False)


(options, args) = parser.parse_args()

###################################################################

def main(options):

    print("Input folder: {}".format(options.inputPklFolder))
    df_files = glob.glob("{}/*.pkl".format(options.inputPklFolder))

    os.system("mkdir -p {}".format(options.plotDir))

    sample = getSample(df_files, options.name)
    runAnalysis(sample, options.plotDir, options.load_DNNmodel, options.features, options.load_CNNmodel)

    print("-----------------------------------------")
    print("DONE!")

def getHistTools(name, plotDir):
    histTools = HistTools(name, plotDir)
    histTools.addHistogram("DNN Discriminant", "DNN score", 100, 0, 1, 1)
    histTools.addHistogram("CNN Discriminant", "CNN score", 100, 0, 1, 1)
    histTools.addHistogram("Yield", "yield", 1, 0, 1, 1)
    histTools.addHistogram('met_tst_et',"MET [GeV]", 120, 250, 1250, 1./GeV)
    histTools.addHistogram('leadTCC_pt', "TCC Jet pT [GeV]", 120, 250, 1250, 1./GeV)
    histTools.addHistogram('leadTCC_eta',"TCC Jet eta", 50, -3.5, 3.5, 1.)
    histTools.addHistogram('leadTCC_phi',"TCC Jet phi", 40,-3.5,3.5,1.)
    histTools.addHistogram('leadTCC_m',"TCC Jet m [GeV]", 20,0,200,1./GeV)
    histTools.addHistogram('leadTCC_tau21',"TCC Jet tau21", 50,0,1,1)
    histTools.addHistogram('leadTCC_D2',"TCC Jet D2",50,0,10,1)
    histTools.addHistogram('leadTCC_C2',"TCC Jet C2",50,0,1,1)
    histTools.addHistogram('leadTCC_nConstit',"TCC Jet nConstit",50,0,50,1)
    histTools.addHistogram('dPhiTCCJetMet',"DeltaPhi(TCC,Met)",50,0,3.5,1)
        
    return histTools

def getInvHChannel(run):
    if run == 346588: return 'ggF'
    elif run == 346600: return 'VBF'
    elif run == 346605 or run == 346606 or run == 346607: return 'VH'
    elif run == 345596: return 'ggZH'
    elif run == 346632 or run == 346633 or run == 346634: return 'ttH'
    print("Run number {} is not from invH!".format(run))
    return ''

def runAnalysis(sample, plotDir, load_DNNmodel, features_index, load_CNNmodel):
    print("------- Running analysis for {} -------".format(sample['name']))

    raw_yield = {'VTagger':0,'DNN':0,'CNN':0}
    w_yield = {'VTagger':0,'DNN':0,'CNN':0}
    histTools_skim = getHistTools(sample['name'] + "_skim", plotDir)
    histTools_VTagger = getHistTools(sample['name'] + "_VTagger", plotDir)
    histTools_DNN = getHistTools(sample['name'] + "_DNN", plotDir)
    histTools_CNN = getHistTools(sample['name'] + "_CNN", plotDir)

    histTools_splitted = {}
    if options.split_channels:
        for channel in ['ggF','VBF','VH','ggZH','ttH']:
            histTools_splitted[channel] = {
                                                'skim':getHistTools(sample['name'] + "_{}_skim".format(channel), plotDir + "_" + channel),
                                                'VTagger':getHistTools(sample['name'] + "_{}_VTagger".format(channel), plotDir + "_" + channel),
                                                'DNN':getHistTools(sample['name'] + "_{}_DNN".format(channel), plotDir + "_" + channel),
                                                'CNN':getHistTools(sample['name'] + "_{}_CNN".format(channel), plotDir + "_" + channel),
            }


    model_dnn, scaler, le = loadModel(load_DNNmodel)
    InputFeatures = GetFeatures(features_index)

    model_cnn = loadCNNModel(load_CNNmodel)
    cnn_mostSignal = -1
    cnn_mostBackground = -1
    th2d_mostSignal = None
    th2d_mostBackground = None

    for i_file,df_path in enumerate(sample['df_files']):
        print("Opening {}  (File {}/{})".format(df_path,i_file+1,len(sample['df_files'])))
        df = pd.read_pickle(df_path)

        df_preselection = ApplyPreselection(df)
        df_preselection = df_preselection.dropna(axis=0, subset = InputFeatures)
        n_events_preselection = df_preselection.shape[0]
        i_event = 0
        for index, row in df_preselection.iterrows():
            if i_event % 10000 == 0 or i_event == n_events_preselection - 1: print("Processing event {} ({:.2f} %)".format(i_event, 100*(i_event+1)/n_events_preselection))
            i_event += 1

            # DNN Discriminant
            dnn_discriminant = -1
            if model_dnn:
                row_features = row[InputFeatures]
                df_features = pd.DataFrame(np.array([row_features]), columns=InputFeatures)
                features_scaled = scaler.transform(df_features[InputFeatures].values)
                dnn_discriminant = model_dnn.predict(features_scaled)

            # CNN Discriminant
            cnn_discriminant = -1
            if model_cnn:
                array_img = GetArrayImage(row['leadTCC_eta'], row['leadTCC_phi'], row['TCCJet_lead_clusters_pt'], row['TCCJet_lead_clusters_eta'], row['TCCJet_lead_clusters_phi'])
                array_img = preprocessImage(array_img)
                cnn_discriminant = model_cnn.predict(np.array([array_img]))
                if cnn_mostSignal == -1 or cnn_mostSignal < cnn_discriminant:
                    cnn_mostSignal = cnn_discriminant
                    th2d_mostSignal = GetTH2DImage(row['leadTCC_eta'], row['leadTCC_phi'], row['TCCJet_lead_clusters_pt'], row['TCCJet_lead_clusters_eta'], row['TCCJet_lead_clusters_phi'])
                if cnn_mostBackground == -1 or cnn_mostBackground > cnn_discriminant:
                    cnn_mostBackground = cnn_discriminant
                    th2d_mostBackground = GetTH2DImage(row['leadTCC_eta'], row['leadTCC_phi'], row['TCCJet_lead_clusters_pt'], row['TCCJet_lead_clusters_eta'], row['TCCJet_lead_clusters_phi'])

            # Skimmed
            histTools_skim.fillHistograms(row, row['weight'])
            histTools_skim.fillHistogram("DNN Discriminant", dnn_discriminant, row['weight'])
            histTools_skim.fillHistogram("CNN Discriminant", cnn_discriminant, row['weight'])
            histTools_skim.fillHistogram("Yield", 0, row['weight'])
            if options.split_channels: 
                histTools_splitted[getInvHChannel(row['run'])]['skim'].fillHistograms(row, row['weight'])
                histTools_splitted[getInvHChannel(row['run'])]['skim'].fillHistogram("DNN Discriminant", dnn_discriminant, row['weight'])
                histTools_splitted[getInvHChannel(row['run'])]['skim'].fillHistogram("CNN Discriminant", cnn_discriminant, row['weight'])
                histTools_splitted[getInvHChannel(row['run'])]['skim'].fillHistogram("Yield", 0, row['weight'])

            # V-Tagger
            D2_isWJet = row['TCCJet_passD2_W50'][0] == 1
            D2_isZJet = row['TCCJet_passD2_Z50'][0] == 1
            Mass_isWJet = row['TCCJet_passMass_W50'][0] == 1
            Mass_isZJet = row['TCCJet_passMass_Z50'][0] == 1
            if D2_isWJet and D2_isZJet and Mass_isWJet and Mass_isZJet:         # Pass V-Tagger
                raw_yield['VTagger'] += 1
                w_yield['VTagger'] += row['weight']
                histTools_VTagger.fillHistograms(row, row['weight'])
                histTools_VTagger.fillHistogram("DNN Discriminant", dnn_discriminant, row['weight'])
                histTools_VTagger.fillHistogram("CNN Discriminant", cnn_discriminant, row['weight'])
                histTools_VTagger.fillHistogram("Yield", 0, row['weight'])
                if options.split_channels: 
                    histTools_splitted[getInvHChannel(row['run'])]['VTagger'].fillHistograms(row, row['weight'])
                    histTools_splitted[getInvHChannel(row['run'])]['VTagger'].fillHistogram("DNN Discriminant", dnn_discriminant, row['weight'])
                    histTools_splitted[getInvHChannel(row['run'])]['VTagger'].fillHistogram("CNN Discriminant", cnn_discriminant, row['weight'])
                    histTools_splitted[getInvHChannel(row['run'])]['VTagger'].fillHistogram("Yield", 0, row['weight'])
            if dnn_discriminant > 0.5:                                          # Pass DNN
                raw_yield['DNN'] += 1
                w_yield['DNN'] += row['weight']
                histTools_DNN.fillHistograms(row, row['weight'])
                histTools_DNN.fillHistogram("DNN Discriminant", dnn_discriminant, row['weight'])
                histTools_DNN.fillHistogram("CNN Discriminant", cnn_discriminant, row['weight'])
                histTools_DNN.fillHistogram("Yield", 0, row['weight'])
                if options.split_channels: 
                    histTools_splitted[getInvHChannel(row['run'])]['DNN'].fillHistograms(row, row['weight'])
                    histTools_splitted[getInvHChannel(row['run'])]['DNN'].fillHistogram("DNN Discriminant", dnn_discriminant, row['weight'])
                    histTools_splitted[getInvHChannel(row['run'])]['DNN'].fillHistogram("CNN Discriminant", cnn_discriminant, row['weight'])
                    histTools_splitted[getInvHChannel(row['run'])]['DNN'].fillHistogram("Yield", 0, row['weight'])
            if cnn_discriminant > 0.5:
                raw_yield['CNN'] += 1
                w_yield['CNN'] += row['weight']
                histTools_CNN.fillHistograms(row, row['weight'])
                histTools_CNN.fillHistogram("DNN Discriminant", dnn_discriminant, row['weight'])
                histTools_CNN.fillHistogram("CNN Discriminant", cnn_discriminant, row['weight'])
                histTools_CNN.fillHistogram("Yield", 0, row['weight'])
                if options.split_channels: 
                    histTools_splitted[getInvHChannel(row['run'])]['CNN'].fillHistograms(row, row['weight'])
                    histTools_splitted[getInvHChannel(row['run'])]['CNN'].fillHistogram("DNN Discriminant", dnn_discriminant, row['weight'])
                    histTools_splitted[getInvHChannel(row['run'])]['CNN'].fillHistogram("CNN Discriminant", cnn_discriminant, row['weight'])
                    histTools_splitted[getInvHChannel(row['run'])]['CNN'].fillHistogram("Yield", 0, row['weight'])

    print("======= Analysis results =======")
    print("----- VTagger -----")
    print("raw_yield = {}".format(raw_yield['VTagger']))
    print("w_yield = {}".format(w_yield['VTagger']))
    print("----- DNN -----")
    print("raw_yield = {}".format(raw_yield['DNN']))
    print("w_yield = {}".format(w_yield['DNN']))
    print("----- CNN -----")
    print("raw_yield = {}".format(raw_yield['CNN']))
    print("w_yield = {}".format(w_yield['CNN']))

    print("======= Plotting =======")
    histTools_skim.plotHistograms()
    histTools_skim.save()
    histTools_VTagger.plotHistograms()
    histTools_VTagger.save()
    histTools_DNN.plotHistograms()
    histTools_DNN.save()
    histTools_CNN.plotHistograms()
    histTools_CNN.save()
    if options.split_channels:
        for channel in ['ggF','VBF','VH','ggZH','ttH']:
            histTools_splitted[channel]['skim'].save()
            histTools_splitted[channel]['VTagger'].save()
            histTools_splitted[channel]['DNN'].save()
            histTools_splitted[channel]['CNN'].save()
    if model_cnn:
        plot2DHistogram(th2d_mostSignal, "eta", "phi", "{}/mostSignalImage".format(options.plotDir))
        plot2DHistogram(th2d_mostBackground, "eta", "phi", "{}/mostBackgroundImage".format(options.plotDir))

if __name__ == '__main__':
    main(options)
