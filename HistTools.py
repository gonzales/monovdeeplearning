import os
from ROOT import gROOT, gStyle, TFile, TH1, TH1D, TH2D, TCanvas
from utils import *
from root_utils import *

class Histogram:
    # def __init__(self, varName, xLabel, n_bins, xlow, xhigh, varFactor):
    def __init__(self, *args):
        if len(args) > 1:
            self.varName = args[0]
            self.varFactor = args[5]
            self.xLabel = args[1]
            self.hist = TH1D("","",args[2],args[3],args[4]) 
        elif isinstance(args[0], TH1D):
            self.hist = args[0]

    

class HistTools:
    def __init__(self, name, outDir):
        self.name = name
        self.outDir = outDir
        self.hists = {}

    def addHistogram(self, varName, xLabel, n_bins, xlow, xhigh, varFactor = 1):
        histogram = Histogram(varName, xLabel, n_bins, xlow, xhigh, varFactor)
        self.hists[varName] = histogram
        print("Added histogram with name {}".format(varName))

    def getHistogram(self, name):
        if name in self.hists.keys():
            return self.hists[name].hist
        else:
            print("Could not find histogram for {}".format(name))

    def fillHistograms(self, row, weight):
        for name,histogram in self.hists.items():
            if histogram.varName in row.keys():
                histogram.hist.Fill(row[histogram.varName] * histogram.varFactor, weight)

    def fillHistogram(self, name, value, weight):
        if name in self.hists.keys():
            self.hists[name].hist.Fill(value * self.hists[name].varFactor, weight)
        else:
            print("The histogram with name {} was not added".format(name))

    def plotHistograms(self):
        if self.outDir == "" :
            print("No outDir defined for HistTools {}".format(self.name))
        else :
            os.system("mkdir -p {}/{}".format(self.outDir, self.name))
            for name,histogram in self.hists.items():
                plotHistogram(histogram.hist, histogram.xLabel, "{}/{}/{}".format(self.outDir, self.name, histogram.varName))

    def save(self):
        os.system("mkdir -p {}/{}".format(self.outDir, self.name))
        fileName = "{}/{}/histograms.root".format(self.outDir, self.name)
        file = TFile(fileName,"RECREATE")
        for name,histogram in self.hists.items():
            histogram.hist.Write(name)
        file.Close()
        print("Histograms stored in {}".format(fileName))

    def loadHistograms(self, root_file_path):
        file = TFile(root_file_path)
        TH1.AddDirectory(0)
        for key in file.GetListOfKeys():
            hist = key.ReadObj()
            if hist.ClassName() == "TH1D":
                histogram = Histogram(hist.Clone(key.GetName()))
                self.hists[key.GetName()] = histogram
        file.Close()



    


