from optparse import OptionParser

parser = OptionParser()

parser.add_option('-i', '--input-folder', type='string', help='Path to input ToyNTuple', dest = "inputFolder")
parser.add_option('-o', '--output-dir', type='string', help='Path to place the output', dest = "outputDir")
parser.add_option('-t', '--treeName', type='string', help='Name of the tree', dest = "treeName", default = "ntuple")
parser.add_option('-c', '--columns', type='int', help='Column group', dest = "columns", default = 0)
parser.add_option('-s', '--selection', type='string', help='Selection to apply', dest = "selection", default = "")
parser.add_option('--chunk-size', type='int', help='Chunk size', dest = "chunksize", default = -1)

(options, args) = parser.parse_args()

import os
import pandas as pd
from root_pandas import read_root
from progressbar import ProgressBar
from utils import *



###################################################################

def main(options):
    print("Reading merged files from: " + options.inputFolder)
    channel = os.path.basename(os.path.normpath(options.inputFolder))
    files = glob.glob("{}/*nominal*root".format(options.inputFolder))

    # columns = None
    columns = ["*LCTopo*","*met*","*run*","*year*","*weight*","n_el_baseline","n_mu_baseline","tau_loose_multiplicity","*trigger*"]
    chunksize = None if options.chunksize == -1 else options.chunksize
    if options.chunksize > 0: chunksize = options.chunksize

    pbar = ProgressBar()
    os.system("mkdir -p {}".format(options.outputDir))
    if options.selection != "":
        options.outputDir += "/{}".format(options.selection) 
        os.system("mkdir -p {}".format(options.outputDir))

    if chunksize:
        n_file = 1
        for df in pbar(read_root(files, options.treeName, columns = columns, chunksize=chunksize)):
            processAndSave(df, options.selection, "{}/{}_part{}.feather".format(options.outputDir,channel,n_file))
            n_file += 1
    else:
        df = read_root(files, options.treeName, columns = columns)
        processAndSave(df, options.selection, "{}/{}.feather".format(options.outputDir,channel))

    print("------------------------------")
    print("DONE!")

def processAndSave(df, selection, fileName):

    leadLCTopoJet_pt = []
    leadLCTopoJet_eta = []
    leadLCTopoJet_phi = []
    leadLCTopoJet_m = []
    leadLCTopoJet_D2 = []
    leadLCTopoJet_C2 = []
    leadLCTopoJet_tau21 = []
    leadLCTopoJet_nConstit = []

    if "Preselection" in selection:
        df = ApplyPreselectionMergedFile(df)
    if "VH" in selection:
        df = df.loc[(df['run'] == 346605) | (df['run'] == 346606) | (df['run'] == 346607)]

    for index, row in df.iterrows():
        leadLCTopoJet_pt.append(getLeadingLCTopoJet(row,"pt"))
        leadLCTopoJet_eta.append(getLeadingLCTopoJet(row,"eta"))
        leadLCTopoJet_phi.append(getLeadingLCTopoJet(row,"phi"))
        leadLCTopoJet_m.append(getLeadingLCTopoJet(row,"m"))
        leadLCTopoJet_D2.append(getLeadingLCTopoJet(row,"D2"))
        leadLCTopoJet_C2.append(getLeadingLCTopoJet(row,"C2"))
        leadLCTopoJet_tau21.append(getLeadingLCTopoJet(row,"tau21"))
        leadLCTopoJet_nConstit.append(getLeadingLCTopoJet(row,"nConstit"))

    df.insert(0,'leadLCTopoJet_pt',leadLCTopoJet_pt)
    df.insert(0,'leadLCTopoJet_eta',leadLCTopoJet_eta)
    df.insert(0,'leadLCTopoJet_phi',leadLCTopoJet_phi)
    df.insert(0,'leadLCTopoJet_m',leadLCTopoJet_m)
    df.insert(0,'leadLCTopoJet_D2',leadLCTopoJet_D2)
    df.insert(0,'leadLCTopoJet_C2',leadLCTopoJet_C2)
    df.insert(0,'leadLCTopoJet_tau21',leadLCTopoJet_tau21)
    df.insert(0,'leadLCTopoJet_nConstit',leadLCTopoJet_nConstit)

    df.reset_index(inplace=True)

    df.to_feather(fileName)
    print("Output stored in: {}".format(fileName))

def getLeadingLCTopoJet(row,attr):
    if (row['n_LCTopoJet'] == 0):
        return -999
    else:
        return row['LCTopoJet_' + attr][0]

if __name__ == '__main__':
    main(options)
